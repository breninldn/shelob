#!/usr/bin/env bash

describe "Term"

source src/term.sh

it_reports_exsiting_function_correctly() {

	test_function() {
		# shellcheck disable=SC2317
		return
	}

	export -f test_function

	function_exists test_function

}

it_reports_non_exsiting_function_correctly() {

	! function_exists test_function

}
