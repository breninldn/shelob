#!/usr/bin/env bash
#shellcheck disable=2154,1117,2034
set -eu

describe "Logger"

before() {
  export TERM='xterm'
  source src/logger.sh
  SHELOB_COLOR='always'
}

it_formats_emergency_message_and_exits() {
  local formatted_prefix="${__red_bg}${__yellow}${__bold}${__underline}[emergency]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${__red}${__bold}${__underline}hello${__reset}")"
  local expected="${formatted_prefix}${formatted_message}"
  set +ex
  output=$(emergency "hello" 2>&1)
  status=$?
  set -ex
  test $status -ne 0
  test "$expected" = "$output"
}

it_formats_alert_message() {
  local message="Hello"
  local formatted_prefix="${__red_bg}${__yellow}[alert]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s" "${__red}${__bold}${message}${__reset}")"
  local expected="${formatted_prefix}${formatted_message}"
  local output
  set +x
  output=$(alert "$message" 2>&1)
  set -x
  test "$expected" = "$output"
}

it_formats_critical_message() {
  local message="Hello"
  local formatted_prefix="${__red}${__underline}${__bold}[critical]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${__red}${message}${__reset}")"
  local expected="${formatted_prefix}${formatted_message}"
  local output
  set +x
  output=$(critical "$message" 2>&1)
  set -x
  test "$expected" = "$output"
}

it_formats_error_message() {
  local message="Hello"
  local formatted_prefix="${__red}${__bold}[error]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${message}")"
  local expected="${formatted_prefix}${formatted_message}"
  local output
  set +x
  output=$(error "$message" 2>&1)
  set -x
  test "$expected" = "$output"
}

it_formats_warning_message() {
  local message="Hello"
  local formatted_prefix="${__yellow}${__bold}[warning]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${message}")"
  local expected="${formatted_prefix}${formatted_message}"

  local output
  set +x
  output=$(warning "$message" 2>&1)
  set -x
  test "$expected" = "$output"
}

it_formats_notice_message() {
  local message="Hello"
  local formatted_prefix="${__blue}${__bold}[notice]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${message}")"
  local expected="${formatted_prefix}${formatted_message}"

  local output
  output=$(notice "$message")
  test "$expected" = "$output"
}

it_formats_info_message() {
  local message="Hello"
  local formatted_prefix="${__green}[info]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${message}")"
  local expected="${formatted_prefix}${formatted_message}"

  local output
  output=$(info "$message")
  test "$expected" = "$output"
}

it_formats_debug_message() {
  SHELOB_LOG_LEVEL=7
  local message="Hello"
  local formatted_prefix="${__cyan_bg}${__black}[debug]:${__reset}"
  local formatted_message
  formatted_message="$(printf " %s\n" "${message}")"
  local expected="${formatted_prefix}${formatted_message}"

  local output
  output=$(debug "$message")
  test "$expected" = "$output"
}

it_only_outputs_enabled_log_messages() {
  SHELOB_LOG_LEVEL=3
  local output
  output=$(info "Info")
  [[ ! $output =~ .*Info.* ]]
  SHELOB_LOG_LEVEL=6
  output=$(info "Info")
  [[ $output =~ .*Info.* ]]
}

it_does_not_output_colors_with_no_tty_connected() {
  SHELOB_COLOR='auto'
  local expected
  expected="$(printf "[info]: Info\n")"
  local output
  output=$(info "Info")
  test "$expected" = "$output"
}

it_prints_timestamp_when_enabled() {
  SHELOB_LOG_TIMESTAMP=true
  SHELOB_COLOR='auto'
  local expected_pattern=$'^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} UTC \[info\]: Info$'
  output=$(info "Info")
  [[ $output =~ $expected_pattern  ]]
}


it_handles_multiple_lines() {
SHELOB_COLOR='auto'
#Trailing new lines are cleared and a single new line at the end is ensured
local input=
input=$(cat << EOF
this
is
a
multiline
text


EOF
)

local expected=
expected="$(printf "[info]: this\nis\na\nmultiline\ntext\n")"
output=$(info "$input")
[[ $output = "$expected" ]]
}
