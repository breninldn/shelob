#!/usr/bin/env bash
#shellcheck disable=SC2154
set -eu

describe "Filesystem"

before() {
     source src/filesystem.sh
     testdir="$(pwd)/tests"
}

it_resolves_current_source_path() {
    [[ $(source_path) = "$testdir/unit-tests/filesystem-test.sh" ]]
}

it_resolves_current_source_dir() {
    [[ $(source_dir) = "$testdir/unit-tests" ]]
}

it_resolves_current_source_file_name() {
    [[ $(source_file_name) = "filesystem-test.sh" ]]
}

it_resolves_executed_source_info() {
    ./tests/utils/sourcefilesystem.sh
}

it_resolves_sourced_source_info() {
    source tests/utils/sourcefilesystem.sh
    [[ $status = 0 ]]
}

it_resolves_executed_link_info() {
    ./tests/utils/sourcelink.sh
}

it_resolves_sourced_link_info() {
    source tests/utils/sourcelink.sh
    [[ $status = 0 ]]
}

it_resolves_absolute_dirname () {
    local dir=
    dir="$(absolute_dirname "./tests/utils/sourcefilesystem.sh")"
    [[ "$dir" = "$testdir/utils" ]]
}
