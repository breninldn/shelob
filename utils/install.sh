#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
shopt -s nullglob

prefix=${PREFIX:-/usr/local/lib}

for l in src/*; do
  install -D "$l" "$prefix/shelob/$(basename "$l")"
done

install -D utils/uninstall.sh "$prefix/shelob/uninstall.sh"


#Install globally if installing with root privilege
if [[ $EUID -eq 0 ]]; then
  echo "Shelob installed globally to $prefix/shelob"
  echo "export SHELOB_HOME=${prefix}/shelob" > /etc/profile.d/shelob.sh
  echo "Login again to start using shelob"
else
  echo "Shelob installed for user $USER to $prefix/shelob"
  echo "**** Add \"SHELOB_HOME=${prefix}/shelob\" to your shell startup file"
fi

echo "Run ${prefix}/shelob/uninstall.sh to uninstall"
