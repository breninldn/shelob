#!/usr/bin/env bash
set -e

wget https://github.com/koalaman/shellcheck/releases/download/stable/shellcheck-stable.linux.x86_64.tar.xz
tar --xz -xvf shellcheck-stable.linux.x86_64.tar.xz
cp shellcheck-stable/shellcheck /usr/bin
shellcheck --version
