#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

shopt -s nullglob

dir="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

for f in "$dir"/*; do
  rm -v "$f"
done

rmdir -v "$dir"

#Remove shell startup file if run as root
if [[ $EUID -eq 0 ]]; then
  rm -f /etc/profile.d/shelob.sh
fi
