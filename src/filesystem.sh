#!/usr/bin/env bash
# shellcheck disable=SC2155

#  Prevent sourcing more then once if already sourced
[ -z "${SHELOB_FILESYSTEM:-}" ] && SHELOB_FILESYSTEM="Y" || return 0

__resolve_links() {
	local path=${1:-}
	local dir=
	# resolve $source until the file is no longer a symlink
	while [ -h "$path" ]; do
		dir="$(cd -P "$(dirname "$path")" && pwd)"
		# if $path was a relative symlink, we need to resolve it relative
		# to the path where the symlink file was located
		path="$(readlink "$path")"
		[[ $path != /* ]] && path="$dir/$path"
	done

	echo "$path"
}

absolute_dirname() {
	local path=${1:-}
	cd -P "$(dirname "$path")" && pwd
}

source_path() {
	local path="$(__resolve_links "${BASH_SOURCE[1]}")"
	dir="$(absolute_dirname "$path")"
	file_name="$(basename "$path")"

	echo "$dir/$file_name"
}

source_dir() {
	local path="$(__resolve_links "${BASH_SOURCE[1]}")"
	absolute_dirname "$path"
}

source_file_name() {
	local path="$(__resolve_links "${BASH_SOURCE[1]}")"
	basename "$path"
}
