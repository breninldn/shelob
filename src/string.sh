#!/usr/bin/env bash

[ -z "${SHELOB_STRING:-}" ] && SHELOB_STRING="Y" || return 0

#######################################
# Test if given string is empty.( blank, spaces, or unbound )
# If no argument is passed test succeeds.
#
# Globals:
#   None
# Arguments:
#    string ($1) : String to test
# Returns:
#   0	 : If string is empty
#   1  : If string is not empty
# Output:
#   None
#######################################
function is_empty() {
	local string=${1:-} # remove all whitespace
	string=${string// /}
	[[ -z $string ]]
}

#######################################
# Test if given string is not empty.( blank, spaces, or unbound )
# If no argument is passed test fails.
#
# Globals:
#   None
# Arguments:
#    string ($1) : String to test
# Returns:
#   0	 : If string is now empty
#   1  : If string is empty
# Output:
#   None
#######################################
function not_empty() {
	! is_empty "$@"
}

#######################################
# Trim trailing space characters
#
# Globals:
#   None
# Arguments:
#    string ($1) : String to rtrim
# Returns:
#   0  : If successful
#   1  : If failure
# Output:
#   String with trailing spaces trimmed
#######################################
function rtrim() {
	local var="$*"
	# remove trailing whitespace characters
	var="${var%"${var##*[![:space:]]}"}"
	echo -n "$var"
}

#######################################
# Trim leading space characters
#
# Globals:
#   None
# Arguments:
#    string ($1) : String to ltrim
# Returns:
#   0  : If successful
#   1  : If failure
# Output:
#   String with leading spaces trimmed
#######################################
function ltrim() {
	local var="$*"
	# remove leading whitespace characters
	var="${var#"${var%%[![:space:]]*}"}"
	echo -n "$var"
}

#######################################
# Trim surrounding space characters
#
# Globals:
#   None
# Arguments:
#    string ($1) : String to trim
# Returns:
#   0  : If successful
#   1  : If failure
# Output:
#   String with surrounding spaces trimmed
#######################################
# http://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
function trim() {
	local var="$*"
	# remove leading whitespace characters
	var="${var#"${var%%[![:space:]]*}"}"
	# remove trailing whitespace characters
	var="${var%"${var##*[![:space:]]}"}"
	echo -n "$var"
}
